#global candidate rc0
%bcond_without toolsonly

Name:     uboot-tools
Version:  2024.07
Release:  3nc1%{?candidate:.%{candidate}}%{?dist}
Epoch:    1
Summary:  U-Boot utilities
License:  GPLv2+ BSD LGPL-2.1+ LGPL-2.0+
URL:      http://www.denx.de/wiki/U-Boot

ExcludeArch: s390x
Source0:  https://ftp.denx.de/pub/u-boot/u-boot-%{version}%{?candidate:-%{candidate}}.tar.bz2
Source1:  aarch64-boards

# This is now legacy, most devices use bootflow, we keep this for the laggards
Patch1:   uefi-distro-load-FDT-from-any-partition-on-boot-device.patch
# Identify VFAT partitions as ESP, allows EFI setvar on our images
Patch2:   uefi-Add-all-options-for-EFI-System-Partitions.patch
# New function to find fdt for loading from disk
Patch3:   uefi-initial-find_fdt_location-for-finding-the-DT-on-disk.patch
# Fedora patches to enable/disable features
Patch4:   disable-VBE-by-default.patch
Patch5:   enable-bootmenu-by-default.patch
# Should be upstream but it's taking time
Patch6:   Add-video-damage-tracking.patch

# Rockchips improvements
Patch10:  rockchip-Enable-preboot-start-for-pci-usb.patch

# Build with OPENSSL_NO_ENGINE
Patch11:  openssl-no-engine.patch

# Asahi patches from gen-asahi-patches.sh
# input: apple: Split off report handling into a separate file
Patch100: https://github.com/AsahiLinux/u-boot/commit/6315e711c4db182397fffed20e2a9996b24b7032.patch#/asahi-6315e711c4db182397fffed20e2a9996b24b7032.patch
# arm: apple: rtkit: Add support for AP power & syslogs
Patch101: https://github.com/AsahiLinux/u-boot/commit/72a46bd89bd5fcd461edd332c82348830a19b689.patch#/asahi-72a46bd89bd5fcd461edd332c82348830a19b689.patch
# arm: apple: rtkit: Add default buffer handlers
Patch102: https://github.com/AsahiLinux/u-boot/commit/4380406063af162986f4f0392374c4739cf199cc.patch#/asahi-4380406063af162986f4f0392374c4739cf199cc.patch
# arm: apple: rtkit: Add a generic RTKit helper driver
Patch103: https://github.com/AsahiLinux/u-boot/commit/f1a235dc1f9d56c20b267a786d3fe8ef0201e4f4.patch#/asahi-f1a235dc1f9d56c20b267a786d3fe8ef0201e4f4.patch
# input: apple: Add support for Apple MTP keyboard
Patch104: https://github.com/AsahiLinux/u-boot/commit/5af2de33b0b25c360bd8971e653fdbe7d7763513.patch#/asahi-5af2de33b0b25c360bd8971e653fdbe7d7763513.patch
# arm: apple: Add MTP keyboard options to defconfig
Patch105: https://github.com/AsahiLinux/u-boot/commit/33765dc99b3e21eaa5d394d4bbab187829eee5d9.patch#/asahi-33765dc99b3e21eaa5d394d4bbab187829eee5d9.patch
# apple: Set up file system firmware loader
Patch106: https://github.com/AsahiLinux/u-boot/commit/504261897e56f96b07a3036b5fb710bbf1b93603.patch#/asahi-504261897e56f96b07a3036b5fb710bbf1b93603.patch
# iopoll: Add readb_poll_sleep_timeout
Patch107: https://github.com/AsahiLinux/u-boot/commit/88c223dd36d11e7d1d74c7fc8fe238d095fe7905.patch#/asahi-88c223dd36d11e7d1d74c7fc8fe238d095fe7905.patch
# usb: xhci-pci: Load ASMedia XHCI controller firmware
Patch108: https://github.com/AsahiLinux/u-boot/commit/8cdfcb9663ba00e54af74f333cb00e023cfa2905.patch#/asahi-8cdfcb9663ba00e54af74f333cb00e023cfa2905.patch
# env: apple: Enable ENV_IS_IN_FAT
Patch109: https://github.com/AsahiLinux/u-boot/commit/a5078187e7932a660280180b9f4c8893126acf31.patch#/asahi-a5078187e7932a660280180b9f4c8893126acf31.patch
# apple: Nail down the EFI system partition
Patch110: https://github.com/AsahiLinux/u-boot/commit/635638229d0f9c7cc42a12a374311bf2d0dd4b52.patch#/asahi-635638229d0f9c7cc42a12a374311bf2d0dd4b52.patch
# scripts/dtc: Add support for floating-point literals
Patch111: https://github.com/AsahiLinux/u-boot/commit/a495e4ede3c11ba2d5c5b28ee9e44250fbf01dfc.patch#/asahi-a495e4ede3c11ba2d5c5b28ee9e44250fbf01dfc.patch
# arm: dts: apple: Update Apple M1 device trees
Patch112: https://github.com/AsahiLinux/u-boot/commit/26635b1550e357db36679db4f40cc5087430be09.patch#/asahi-26635b1550e357db36679db4f40cc5087430be09.patch
# arm: dts: apple: Add Apple M1 Pro/Max/Ultra device trees
Patch113: https://github.com/AsahiLinux/u-boot/commit/0194c9bd8c5dc7a929fd971fec3cdf7a72f6364f.patch#/asahi-0194c9bd8c5dc7a929fd971fec3cdf7a72f6364f.patch
# arm: dts: apple: Add Apple M2 device trees
Patch114: https://github.com/AsahiLinux/u-boot/commit/e8614e16512f2662e03981723e6deefd4dffe710.patch#/asahi-e8614e16512f2662e03981723e6deefd4dffe710.patch
# arm: dts: apple: Add Apple M2 Pro/Max device trees
Patch115: https://github.com/AsahiLinux/u-boot/commit/05bd64d9fe21a95c022be6e014c6199a3322abee.patch#/asahi-05bd64d9fe21a95c022be6e014c6199a3322abee.patch
# arm: apple: rtkit: Add OSLog buffer support
Patch116: https://github.com/AsahiLinux/u-boot/commit/f6ae3a2ea8bf3d938db4638c6b369d2a5123b518.patch#/asahi-f6ae3a2ea8bf3d938db4638c6b369d2a5123b518.patch
# arm: apple: rtkit: Add endpoint field to buffers
Patch117: https://github.com/AsahiLinux/u-boot/commit/48f38cfd76d43bd017b175f204eabef3beccc20d.patch#/asahi-48f38cfd76d43bd017b175f204eabef3beccc20d.patch
# arm: apple: rtkit: Support allocating OSLog out of SRAM in helper
Patch118: https://github.com/AsahiLinux/u-boot/commit/e04c7f2c07dc28aad20a3cce13935918fe9df98b.patch#/asahi-e04c7f2c07dc28aad20a3cce13935918fe9df98b.patch
# efi_loader: prefer EFI system partition
Patch119: https://github.com/AsahiLinux/u-boot/commit/03b87bcbd4c89827dc8bf579ef0ca993210db73f.patch#/asahi-03b87bcbd4c89827dc8bf579ef0ca993210db73f.patch
# apple_m1_defconfig: Disable EFI variable store
Patch120: https://github.com/AsahiLinux/u-boot/commit/2a9d0d6b3f968b29b79265e59b1d1d22b794c20f.patch#/asahi-2a9d0d6b3f968b29b79265e59b1d1d22b794c20f.patch
# usb: xhci: Add more debugging
Patch121: https://github.com/AsahiLinux/u-boot/commit/d9ec4ec96d95064627b88ee0f1b309bcdcbe4e63.patch#/asahi-d9ec4ec96d95064627b88ee0f1b309bcdcbe4e63.patch
# usb: storage: Clear endpoint stalls properly
Patch122: https://github.com/AsahiLinux/u-boot/commit/5649f4d989c5e35827e3e19642f0fc711660b9f0.patch#/asahi-5649f4d989c5e35827e3e19642f0fc711660b9f0.patch
# usb: Pass through timeout to drivers
Patch123: https://github.com/AsahiLinux/u-boot/commit/2e1dea9f71062ee2cd4eb213c01d90cce7f11c20.patch#/asahi-2e1dea9f71062ee2cd4eb213c01d90cce7f11c20.patch
# usb: xhci: Hook up timeouts
Patch124: https://github.com/AsahiLinux/u-boot/commit/892928848bc39bbf836937db85d66c4346cef982.patch#/asahi-892928848bc39bbf836937db85d66c4346cef982.patch
# scsi: Fix a bunch of SCSI definitions.
Patch125: https://github.com/AsahiLinux/u-boot/commit/0740ca6ce9e20b4e6158c3fbcd2ae63841209f23.patch#/asahi-0740ca6ce9e20b4e6158c3fbcd2ae63841209f23.patch
# usb: storage: Increase read/write timeout
Patch126: https://github.com/AsahiLinux/u-boot/commit/32a25e412668f25770b7731d0f05df1153881d4d.patch#/asahi-32a25e412668f25770b7731d0f05df1153881d4d.patch
# usb: storage: Implement 64-bit LBA support
Patch127: https://github.com/AsahiLinux/u-boot/commit/2b5269edb27565e133e4d8d57e35d6dad5a23992.patch#/asahi-2b5269edb27565e133e4d8d57e35d6dad5a23992.patch
# usb: Ignore endpoints in non-zero altsettings
Patch128: https://github.com/AsahiLinux/u-boot/commit/155fa255417684d6c3fe94f19d5d26319ed86d5f.patch#/asahi-155fa255417684d6c3fe94f19d5d26319ed86d5f.patch
# video: console: Select default font based on video_priv.font_size
Patch129: https://github.com/AsahiLinux/u-boot/commit/e6a311c020a96487fa458774659c4cb278e40b24.patch#/asahi-e6a311c020a96487fa458774659c4cb278e40b24.patch
# video: simplefb: HACK: Set video font size
Patch130: https://github.com/AsahiLinux/u-boot/commit/7112d6e2264b133f2b11d47df92de78d027e0e89.patch#/asahi-7112d6e2264b133f2b11d47df92de78d027e0e89.patch
# configs: apple: Do not show the boot menu automatically
Patch131: https://github.com/AsahiLinux/u-boot/commit/5de2bdeb8d91736d3122d26cf34f0d7cc9d5c771.patch#/asahi-5de2bdeb8d91736d3122d26cf34f0d7cc9d5c771.patch
# FEDORA: configs: apple: Disable AUTOBOOT_KEYED
Patch132: https://github.com/AsahiLinux/u-boot/commit/5e1506a651a55407ddccfeb60687edf3e3abaea9.patch#/asahi-5e1506a651a55407ddccfeb60687edf3e3abaea9.patch

# arm: apple: change boot order, USB first
Patch1000: asahi-change-boot-order-usb-first.patch
Patch1001: asahi-try-usb-bootflow-first.patch

BuildRequires:  bc
BuildRequires:  bison
BuildRequires:  dtc
BuildRequires:  flex
BuildRequires:  gcc
BuildRequires:  gnutls-devel
BuildRequires:  libuuid-devel
BuildRequires:  make
BuildRequires:  ncurses-devel
BuildRequires:  openssl-devel
BuildRequires:  perl-interpreter
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-libfdt
BuildRequires:  SDL2-devel
BuildRequires:  swig
%if %{with toolsonly}
%ifarch aarch64
BuildRequires:  arm-trusted-firmware-armv8
BuildRequires:  python3-pyelftools
%endif
%endif
Requires:       dtc

%description
This package contains a few U-Boot utilities - mkimage for creating boot images
and fw_printenv/fw_setenv for manipulating the boot environment variables.

%if %{with toolsonly}
%ifarch aarch64
%package     -n uboot-images-armv8
Summary:     U-Boot firmware images for aarch64 boards
BuildArch:   noarch

%description -n uboot-images-armv8
U-Boot firmware binaries for aarch64 boards
%endif
%endif

%prep
%autosetup -p1 -n u-boot-%{version}%{?candidate:-%{candidate}}

cp %SOURCE1 .

%build
mkdir builds

%make_build HOSTCC="gcc $RPM_OPT_FLAGS" CROSS_COMPILE="" tools-only_defconfig O=builds/
%make_build HOSTCC="gcc $RPM_OPT_FLAGS" CROSS_COMPILE="" tools-all O=builds/

%if %{with toolsonly}
%ifarch aarch64
for board in $(cat %{_arch}-boards)
do
  echo "Building board: $board"
  mkdir builds/$(echo $board)/

  # ATF selection, needs improving, suggestions of ATF SoC to Board matrix welcome
  sun50i=(a64-olinuxino a64-olinuxino-emmc amarula_a64_relic bananapi_m2_plus_h5 bananapi_m64 libretech_all_h3_cc_h5 nanopi_a64 nanopi_neo2 nanopi_neo_plus2 oceanic_5205_5inmfd orangepi_pc2 orangepi_prime orangepi_win orangepi_zero_plus orangepi_zero_plus2 pine64-lts pine64_plus pinebook pinephone pinetab sopine_baseboard teres_i)
  if [[ " ${sun50i[*]} " == *" $board "* ]]; then
    echo "Board: $board using sun50i_a64"
    cp /usr/share/arm-trusted-firmware/sun50i_a64/bl31.bin builds/$(echo $board)/atf-bl31
  fi
  sun50h6=(beelink_gs1 emlid_neutis_n5_devboard orangepi_3 orangepi_lite2 orangepi_one_plus pine_h64 tanix_tx6)
  if [[ " ${sun50h6[*]} " == *" $board "* ]]; then
    echo "Board: $board using sun50i_h6"
    cp /usr/share/arm-trusted-firmware/sun50i_h6/bl31.bin builds/$(echo $board)/atf-bl31
  fi
  sun50i_h616=(orangepi_zero2 orangepi_zero3 transpeed-8k618-t x96_mate)
  if [[ " ${sun50i_h616[*]} " == *" $board "* ]]; then
    echo "Board: $board using sun50i_h616"
    cp /usr/share/arm-trusted-firmware/sun50i_h616/bl31.bin builds/$(echo $board)/atf-bl31
  fi
  rk3328=(evb-rk3328 nanopi-r2c-plus-rk3328 nanopi-r2c-rk3328 nanopi-r2s-rk3328 orangepi-r1-plus-lts-rk3328 orangepi-r1-plus-rk3328 roc-cc-rk3328 rock64-rk3328 rock-pi-e-rk3328)
  if [[ " ${rk3328[*]} " == *" $board "* ]]; then
    echo "Board: $board using rk3328"
    cp /usr/share/arm-trusted-firmware/rk3328/bl31.elf builds/$(echo $board)/atf-bl31
  fi
  rk3399=(eaidk-610-rk3399 evb-rk3399 ficus-rk3399 firefly-rk3399 khadas-edge-captain-rk3399 khadas-edge-rk3399 khadas-edge-v-rk3399 leez-rk3399 nanopc-t4-rk3399 nanopi-m4-2gb-rk3399 nanopi-m4b-rk3399 nanopi-m4-rk3399 nanopi-neo4-rk3399 nanopi-r4s-rk3399 orangepi-rk3399 pinebook-pro-rk3399 pinephone-pro-rk3399 puma-rk3399 rock960-rk3399 rock-pi-4c-rk3399 rock-pi-4-rk3399 rock-pi-n10-rk3399pro rockpro64-rk3399 roc-pc-mezzanine-rk3399 roc-pc-rk3399)
  if [[ " ${rk3399[*]} " == *" $board "* ]]; then
    echo "Board: $board using rk3399"
    cp /usr/share/arm-trusted-firmware/rk3399/* builds/$(echo $board)/
    cp builds/$(echo $board)/bl31.elf builds/$(echo $board)/atf-bl31
  fi
  # End ATF

  BINMAN_ALLOW_MISSING=1 make $(echo $board)_defconfig O=builds/$(echo $board)/
  BINMAN_ALLOW_MISSING=1 %make_build HOSTCC="gcc $RPM_OPT_FLAGS" CROSS_COMPILE="" O=builds/$(echo $board)/

done

%endif
%endif

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_datadir}/uboot/

%if %{with toolsonly}
%ifarch aarch64
for board in $(ls builds)
do
 for file in u-boot.bin u-boot.img u-boot-dtb.img u-boot.itb u-boot-sunxi-with-spl.bin u-boot-rockchip-spi.bin u-boot-rockchip.bin idbloader.img idbloader-spi.img spl/boot.bin
 do
  if [ -f builds/$(echo $board)/$(echo $file) ]; then
    install -pD -m 0644 builds/$(echo $board)/$(echo $file) %{buildroot}%{_datadir}/uboot/$(echo $board)/$(echo $file)
  fi
 done
done

# For Apple M-series we also need the nodtb variant
install -pD -m 0644 builds/apple_m1/u-boot-nodtb.bin %{buildroot}%{_datadir}/uboot/apple_m1/u-boot-nodtb.bin

# Bit of a hack to remove binaries we don't use as they're large
for board in $(ls builds)
do
  rm -f %{buildroot}%{_datadir}/uboot/$(echo $board)/u-boot.dtb
  if [ -f %{buildroot}%{_datadir}/uboot/$(echo $board)/u-boot-sunxi-with-spl.bin ]; then
    rm -f %{buildroot}%{_datadir}/uboot/$(echo $board)/u-boot{,-dtb}.*
    rm -f %{buildroot}%{_datadir}/uboot/$(echo $board)/sunxi-spl.bin
  fi
  if [ -f %{buildroot}%{_datadir}/uboot/$(echo $board)/idbloader.img ]; then
    rm -f %{buildroot}%{_datadir}/uboot/$(echo $board)/u-boot.bin
    rm -f %{buildroot}%{_datadir}/uboot/$(echo $board)/u-boot{,-dtb}.img
  fi
done
%endif
%endif

for tool in dumpimage env/fw_printenv fit_check_sign fit_info gdb/gdbcont gdb/gdbsend gen_eth_addr gen_ethaddr_crc ifwitool img2srec kwboot mkeficapsule mkenvimage mkimage mksunxiboot ncb proftool sunxi-spl-image-builder
do
install -p -m 0755 builds/tools/$tool %{buildroot}%{_bindir}
done
install -p -m 0644 doc/mkimage.1 %{buildroot}%{_mandir}/man1

install -p -m 0755 builds/tools/env/fw_printenv %{buildroot}%{_bindir}
( cd %{buildroot}%{_bindir}; ln -sf fw_printenv fw_setenv )

%files
%doc README doc/develop/distro.rst doc/README.gpt
%doc doc/develop/uefi doc/usage doc/arch/arm64.rst
%{_bindir}/*
%{_mandir}/man1/mkimage.1*

%if %{with toolsonly}
%ifarch aarch64
%files -n uboot-images-armv8
%dir %{_datadir}/uboot/
%{_datadir}/uboot/*
%endif
%endif

%changelog
* Mon Sep 30 2024 NoisyCoil <noisycoil@tutanota.com> - 1:2024.07-3nc1
- Rebuild with USB-first patches

* Sun Sep 29 2024 Davide Cavalca <dcavalca@fedoraproject.org> - 1:2024.07-3
- Build with OPENSSL_NO_ENGINE

* Sat Sep 28 2024 NoisyCoil <noisycoil@tutanota.com> - 1:2024.07-2nc1
- Refresh USB-first patches

* Sat Sep 28 2024 Davide Cavalca <dcavalca@fedoraproject.org> - 1:2024.07-2
- Unconditionally build images
- Add missing changelog entry

* Wed Aug 28 2024 Janne Grunau <janne-fdr@jannau.net> - 1:2024.07-1
- Import asahi patches for 2024.07

* Tue Jul 23 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.07-1
- Update to 2024.07

* Sat Jul 20 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1:2024.07-0.3.rc4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_41_Mass_Rebuild

* Tue Jun 18 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.07-0.2.rc4
- Update to 2024.07 RC4

* Sat May 25 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.07-0.1.rc3
- Update to 2024.07 RC3

* Fri Apr 26 2024 NoisyCoil <noisycoil@tutanota.com> - 1:2024.04-4nc2
- Try usb bootflow first

* Fri Apr 26 2024 NoisyCoil <noisycoil@tutanota.com> - 1:2024.04-4nc1
- New upstream release
- Refresh patches

* Fri Apr 19 2024 Janne Grunau <janne-fdr@jannau.net> - 1:2024.04-4
- restore boot behavior on Apple silicon systems (disable AUTOBOOT_KEYED)

* Fri Apr 19 2024 Janne Grunau <janne-fdr@jannau.net> - 1:2024.04-3
- fix dwc2 build

* Thu Apr 18 2024 Janne Grunau <janne-fdr@jannau.net> - 1:2024.04-2
- Import Asahi Linux patches for Apple Silicon support

* Wed Apr 03 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.04-1
- Update to 2024.04 GA
- Rockchip rk3328 USB fixes

* Wed Mar 27 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.04-0.8.rc5
- Update to 2024.04 RC5

* Thu Mar 21 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.04-0.7.rc4
- Updated patch for DTB loading

* Sun Mar 17 2024 NoisyCoil <noisycoil@tutanota.com> - 2023.07-6nc2
- Disable bootmgr when booting from USB drives

* Sat Mar 16 2024 NoisyCoil <noisycoil@tutanota.com> - 2023.07-6nc
- Change boot order: USB first, then NVME

* Fri Mar 15 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.04-0.6.rc4
- Updated fix for FDT load

* Wed Mar 13 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.04-0.5.rc4
- Fixes for Rockchip rk3399 autoboot

* Tue Mar 12 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.04-0.4.rc4
- Update to 2024.04 RC4
- Initial fix for loading DT off /boot (rhbz 2247873)

* Thu Feb 29 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.04-0.3.rc3
- Update to 2024.04 RC3
- Enable a number of new upstream devices
- Upstream now builds Rockchip SPI artifacts
- Various cleanups
- Fix ESP partition detection to enable EFI vars

* Wed Feb 14 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.04-0.2.rc2
- Update to 2024.04 RC2

* Sat Jan 27 2024 Fedora Release Engineering <releng@fedoraproject.org> - 1:2024.01-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Mon Jan 08 2024 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.01-1
- Update to 2024.01

* Thu Dec 07 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.01-0.2.rc4
- Update to 2024.01 RC4
- Rebase SMBIOS patch to latest rev

* Fri Nov 24 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2024.01-0.1.rc3
- Update to 2024.01 RC3

* Tue Oct 31 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 1:2023.10-0.9
- Rebuild

* Mon Oct 23 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.10-0.8
- Further potential upstream fixes

* Mon Oct 02 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.10-0.7
- Update to 2023.10 GA
- Some upstream fixes

* Mon Aug 21 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.10-0.4.rc3
- Update to 2023.10 RC3

* Mon Aug 21 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.10-0.3.rc2
- Add patch to speed up firmware UEFI video output

* Sat Aug 19 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.10-0.2.rc2
- Add patch for Raspberry Pi boot

* Fri Aug 18 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.10-0.1.rc2
- Update to 2023.10 RC2

* Sat Jul 22 2023 Fedora Release Engineering <releng@fedoraproject.org> - 2023.07-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_39_Mass_Rebuild

* Tue Jul 11 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.07-1
- Update to 2023.07 GA

* Fri Jun 16 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.07-0.4.rc4
- Disable NFS by default

* Mon Jun 12 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.07-0.3.rc4
- Update to 2023.07 RC4

* Sun Jun 11 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.07-0.2.rc3
- Update to 2023.07 RC3
- Fixes for the Pinephone Pro, RockPro64

* Wed May 17 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.07-0.1.rc2
- Update to 2023.07 RC2

* Tue Apr 04 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.04-1
- Update to 2023.04 GA

* Tue Mar 28 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.04-0.4.rc5
- Update to 2023.04 RC5
- Drop upstreamed patches
- Rockchip boot fixes

* Tue Mar 14 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.04-0.3.rc4
- Update to 2023.04 RC4

* Fri Feb 17 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.04-0.2.rc2
- Update to 2023.04 RC2

* Tue Jan 31 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.04-0.1.rc1
- Update to 2023.04 RC1
- Drop bmp_logo tool

* Sat Jan 21 2023 Fedora Release Engineering <releng@fedoraproject.org> - 2023.01-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_38_Mass_Rebuild

* Wed Jan 18 2023 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.01-1
- Update to 2023.01 GA

* Sat Dec 31 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.01-0.4.rc4
- Update PinePhone Pro to latest rev

* Tue Dec 20 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.01-0.3.rc4
- Update to 2023.01 RC4

* Mon Dec 05 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.01-0.2.rc3
- Update to 2023.01 RC3

* Thu Nov 24 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2023.01-0.1.rc2
- Update to U-Boot 2023.01 RC2
- Update Pinephone Pro patches

* Mon Oct 10 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.10-1
- Update to 2022.10 GA

* Tue Sep 06 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.10-0.6.rc4
- Update SMBIOS patch

* Tue Sep 06 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.10-0.5.rc4
- Update to 2022.10 RC4
- Fix for booting Rockchip devices from NVME

* Tue Aug 23 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.10-0.4.rc3
- Update to 2022.10 RC3

* Mon Aug 22 2022 Davide Cavalca <dcavalca@fedoraproject.org> - 2022.10-0.3.rc1
- Install nodtb variant for Apple M1 (rhbz#2068958)

* Tue Aug 16 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.10-0.2.rc1
- Fix for DT property propogation via firmware

* Thu Jul 28 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.10-0.1.rc1
- Update to 2022.10 RC1
- Enable LTO for firmware builds

* Sat Jul 23 2022 Fedora Release Engineering <releng@fedoraproject.org> - 2022.07-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_37_Mass_Rebuild

* Sun Jul 17 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.07-1
- Update to 2022.07 GA

* Mon Jul 04 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.07-0.6.rc6
- Update to 2022.07 RC6

* Mon Jun 20 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.07-0.5.rc5
- Update to 2022.07 RC5

* Sun Jun 12 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.07-0.4.rc4
- Update to 2022.07 RC4
- Some minor Rockchips device fixes

* Wed May 25 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.07-0.3.rc3
- Update to 2022.07 RC3

* Sat May 14 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.07-0.2.rc2
- Update to 2022.07 RC2

* Tue Apr 26 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.07-0.1.rc1
- Update to 2022.07 RC1

* Mon Apr 04 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.04-1
- Update to 2022.04 GA

* Mon Mar 28 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.04-0.4.rc5
- Update to 2022.04 RC5

* Tue Mar 08 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.04-0.3.rc3
- Update to 2022.04 RC3
- Enable new Rockchip devices

* Tue Feb 15 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.04-0.2.rc2
- Update to 2022.04 RC2

* Wed Feb 02 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.04-0.1.rc1
- Update to 2022.04 RC1

* Sat Jan 22 2022 Fedora Release Engineering <releng@fedoraproject.org> - 2022.01-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Mon Jan 10 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.01-1
- Update to 2022.01

* Wed Jan 05 2022 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.01-0.3.rc4
- Upstream fixes for PHY and UEFI

* Mon Dec 20 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.01-0.2.rc4
- Update to 2022.01 RC4

* Mon Nov 15 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2022.01-0.1.rc2
- Update to 2022.01 RC2

* Mon Nov 15 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-3
- Fixes for rk3399 devices

* Thu Oct 14 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-2
- Fix booting from MMC for Rockchip 3399 (rhbz #2014182)
- Enable new rk3399 devices (Leez, NanoPi-M4B, NanoPi-4S, NanoPi-T4) (rhbz #2009126)

* Mon Oct 04 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-1
- Update to 2021.10

* Mon Sep 27 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-0.7.rc5
- Update to 2021.10 RC5

* Wed Sep 15 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-0.6.rc4
- Update to 2021.10 RC4
- Proposed fix for RPi MMC clock issue

* Tue Sep 14 2021 Sahana Prasad <sahana@redhat.com> - 2021.10-0.6.rc3
- Rebuilt with OpenSSL 3.0.0

* Mon Aug 30 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-0.5.rc3
- Update to 2021.10 RC3

* Tue Aug 24 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-0.4.rc2
- Fix for Raspberry Pi firmware properties

* Mon Aug 23 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-0.3.rc2
- Fix for rockchip SPI

* Mon Aug 16 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-0.2.rc2
- Update to 2021.10 RC2

* Sun Aug 08 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.10-0.1.rc1
- Update to 2021.10 RC1

* Thu Jul 22 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.07-2
- Fix regression for Rockchip devices running firmware from SPI flash

* Mon Jul 05 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.07-1
- Update to 2021.07 GA

* Mon Jun 28 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.07-0.6.rc5
- Update to 2021.07 RC5
- Build SPI fash images for ROC-PC-RK3399

* Mon Jun 07 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.07-0.5.rc4
- Update to 2021.07 RC4

* Sat Jun 05 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.07-0.4.rc3
- Fix AllWinner devices booting from mSD/MMC

* Tue May 25 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.07-0.3.rc3
- Update to 2021.07 RC3
- Build against ATF 2.5 GA

* Thu May 13 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.07-0.2.rc2
- Build against new ATF 2.5-rc1

* Mon May 10 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.07-0.1.rc2
- Update to 2021.07 RC2

* Wed Apr 28 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.04-3
- Upstream fix for console regression (rhbz 1946278)
- Fix for fallback.efi crash (rhbz 1733817)

* Wed Apr 21 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.04-2
- Revert keyboard console regression change (rhbz 1946278)

* Sun Apr 18 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.04-1
- Update to 2021.04 GA
- Fix DTB load check (rhbz 1946278)
- Build Rockchip SPI support as idbloader.spi
- Fixes for Rockchip devices
- Build Turris Omnia for MMC/SPI/UART

* Wed Mar 17 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.04-0.6.rc4
- Update to 2021.04 RC4
- Move to upstream fix for SMP on RPi3B and RPi3B+

* Sat Mar 13 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.04-0.5.rc3
- Fix for SMP on RPi3B and RPi3B+
- Initial support for Pinephone 3Gb edition

* Mon Mar 08 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.04-0.4.rc3
- Update to 2021.04 RC3

* Tue Feb 16 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.04-0.3.rc2
- Update to 2021.04 RC2

* Mon Feb 15 2021 Dennis Gilmore <dennis@ausil.us>
- build spi and uart images in addition to mmc for helios4 and clearfog

* Wed Feb 10 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.04-0.2.rc1
- Fixes for network issues on some Allwinner devices

* Mon Feb 01 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.04-0.1.rc1
- Update to 2021.04 RC1
- Add new upstream devices

* Wed Jan 27 2021 Fedora Release Engineering <releng@fedoraproject.org> - 2021.01-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Mon Jan 11 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.01-1
- Update to 2021.01 GA
- Updates for Raspberry Pi 4 Series of devices

* Tue Jan  5 2021 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.01-0.5.rc5
- Update to 2021.01 RC5

* Sun Dec 27 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.01-0.4.rc4
- Update to 2021.01 RC4
- Latest RPi-400/CM4 support patch

* Tue Dec 15 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.01-0.3.rc3
- Update to 2021.01 RC3
- Latest RPi-400/CM4 support patch
- Re-enable previously disabled device support

* Mon Dec 14 2020 Javier Martinez Canillas <javierm@redhat.com> - 2021.01-0.2.rc2
- Fix a "scan_dev_for_efi" not defined error

* Sun Nov 22 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2021.01-0.1.rc2
- Update to 2021.01 RC2
- Latest Pinebook Pro display patches
- Initial RPi-400 support patch
- Update Fedora specific patches

* Sun Nov  8 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.10-3
- Fix SPI on Rockchip devices
- Latest Pinebook Pro display patches
- Fix Keyboard and USB-A ports on Pinebook Pro

* Wed Oct 28 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.10-2
- Fix kernel installs for non EBBR systems
- Fix for wired networks on some Allwinner devices

* Tue Oct 06 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.10-1
- Update to 2020.10

* Sun Sep 27 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.10-0.6.rc5
- Initial support for display output on Pinebook Pro

* Tue Sep 22 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.10-0.5.rc5
- Update to 2020.10 RC5

* Wed Sep 09 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.10-0.4.rc4
- Update to 2020.10 RC4

* Wed Aug 19 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.10-0.3.rc2
- Enable a number of new Rockchip devices

* Mon Aug 10 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.10-0.2.rc2
- Update to 2020.10 RC2

* Tue Jul 28 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.10-0.1.rc1
- 2020.10 RC1

* Tue Jul 14 2020 Tom Stellard <tstellar@redhat.com> - 2020.07-2
- Use make macros
- https://fedoraproject.org/wiki/Changes/UseMakeBuildInstallMacro

* Mon Jul 06 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.07-1
- 2020.07 GA

* Tue Jun 23 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.07-0.5.rc5
- 2020.07 RC5

* Thu Jun 18 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.07-0.4.rc4
- Update various patches to latest upstream

* Wed Jun 10 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.07-0.3.rc4
- 2020.07 RC4
- Minor updates and other fixes

* Tue May 12 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.07-0.2.rc2
- 2020.07 RC2
- Minor device updates

* Wed Apr 29 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.07-0.1.rc1
- 2020.07 RC1

* Tue Apr 21 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.04-4
- Initial support for USB on Rasperry Pi 4

* Tue Apr 21 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.04-3
- Ship u-boot-rockchip.bin for SPI flash

* Mon Apr 20 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.04-2
- Fix ATF for new aarch64 devices
- Fix Wandboard board detection (rhbz 1825247)
- Fix mSD card on RockPro64
- Enable (inital) Pinebook Pro

* Tue Apr 14 2020 Peter Robinson <pbrobinson@fedoraproject.org> - 2020.04-1
- 2020.04

* Tue Apr  7 2020 Peter Robinson <pbrobinson@fedoraproject.org> 2020.04-0.7-rc5
- 2020.04 RC5

* Tue Mar 31 2020 Peter Robinson <pbrobinson@fedoraproject.org> 2020.04-0.6-rc4
- 2020.04 RC4
- Updates for NVIDIA Jetson platforms
- Support RNG for random seed for KASLR on some Rockchip devices

* Thu Mar 26 2020 Peter Robinson <pbrobinson@fedoraproject.org> 2020.04-0.5-rc3
- Fix ext4 alignment issue seen on some NXP i.MX devices

* Wed Feb 26 2020 Peter Robinson <pbrobinson@fedoraproject.org> 2020.04-0.4-rc3
- 2020.04 RC3

* Thu Feb 13 2020 Peter Robinson <pbrobinson@fedoraproject.org> 2020.04-0.3-rc2
- 2020.04 RC2

* Sun Feb  2 2020 Peter Robinson <pbrobinson@fedoraproject.org> 2020.04-0.2-rc1
- Update genet NIC driver

* Wed Jan 29 2020 Peter Robinson <pbrobinson@fedoraproject.org> 2020.04-0.1-rc1
- 2020.04 RC1

* Tue Jan  7 2020 Peter Robinson <pbrobinson@fedoraproject.org> 2020.01-1
- 2020.01
