#!/bin/sh

# Run in a checkout of https://github.com/AsahiLinux/u-boot with the commit to start from

outdir=$(dirname "$0")
commit="$1"
count=100

git log --reverse --pretty=format:'%H %s' --no-color HEAD "^${commit}" | while read -r line || [ -n "$line" ]
do
  hash="$(echo "$line" | cut -f1 -d\ )"
  msg="$(echo "$line" | cut -f2- -d\ )"
  
  echo "# ${msg}"
  echo "Patch${count}: https://github.com/AsahiLinux/u-boot/commit/${hash}.patch#/asahi-${hash}.patch"
  count=$((count+=1))

  git format-patch --no-numbered --no-signature -n1 ${hash} --output=${outdir}/asahi-${hash}.patch
done
